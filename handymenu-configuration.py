#!/usr/bin/python3

import sys
from os.path import isdir

from lib.handymenu_configuration import *

if __name__ == "__main__":
    arg         = None
    config_path = None
    verbose     = False
    if len(sys.argv) > 1:
        arg = sys.argv[1]
    if (
        len(sys.argv) > 2
        and '-v' != sys.argv[2]
        and '--verbose' != sys.argv[2]
    ):
        print('-v' != sys.argv[2])
        config_path = sys.argv[2]
        if not isdir(config_path):
            print(
                'Ce dossier de config "%s" n\'existe pas !' % config_path
            )
            exit(0)
    if '-v' in sys.argv or '--verbose' in sys.argv:
        verbose = True
    main(arg, config_path, verbose)
