import os
import copy

from gi.repository import Gtk, Gdk

from .desktop import get_info_desktop
from .hm_utils import appfinder
from .dialog.edit_app import EditAppDialog


class AppConfig():
    def __init__(self, utils, config, APP_MARGIN, APP_SIZE):
        self.utils = utils
        self.config = config
        self.APP_MARGIN = APP_MARGIN
        self.APP_SIZE = APP_SIZE

    def edit(self, widget, event, section, app, callback_restart):
        if event.type != Gdk.EventType._2BUTTON_PRESS:
            return
        edit_app_dialog = EditAppDialog(self.utils, self.config, section, app, callback_restart)
        event = edit_app_dialog.run()
        if event == Gtk.ResponseType.DELETE_EVENT:
            edit_app_dialog.destroy()

    def add_item_to_section(self, name, cmd, icon, generic, section, callback_restart):
        app = {'name' : name, 'icon' : icon, 'cmd' : cmd, 'generic' : generic}
        self.utils.add_app(self.config, section, app)
        callback_restart()

    def on_drag_data_received(self, widget, context, x, y, selection, target_type, timestamp, section, callback_restart):
        # replace URL spaces
        data = selection.get_data().strip().replace(b'%20', b' ')
        if data.startswith(b'file://'):
            f = data.replace(b'file://', b'').strip()
            if os.path.isdir(f): # parse directories
                name = os.path.basename(f)
                cmd = 'exo-open --launch FileManager "{}"'.format(f)
                icon = "folder"
                self.add_item_to_section(name, cmd, icon, None, section)
            elif os.path.isfile(f):
                if data.endswith(b'.desktop'):
                    name, cmd, icon, generic = get_info_desktop(f)
                    # with icon
                    self.add_item_to_section(name, cmd, icon, generic, section, callback_restart)
                else:
                    name = os.path.basename(f)
                    cmd = 'exo-open "{}"'.format(f)
                    # without icon
                    self.add_item_to_section(name, cmd, "empty", None, section, callback_restart)
        # Url case
        elif (
            data.startswith(b'http://')
            or data.startswith(b'https://')
            or data.startswith(b'ftp://')
        ):
            name = data.split(b'/')[2]
            cmd = "exo-open --launch WebBrowser {}".format(data)
            self.add_item_to_section(name, cmd, "text-html", "Lien vers une url", section, callback_restart)

        widget.destroy()

    def add(self, section, window, callback_restart):
        w = Gtk.Dialog(parent=window)
        w.connect("delete_event", callback_restart)
        w.set_size_request(360, 150)

        TARGET_TYPE_URI_LIST = 80
        dnd_list = [
            Gtk.TargetEntry.new(
                'text/uri-list', 0, TARGET_TYPE_URI_LIST
            )
        ]
        w.drag_dest_set(
            Gtk.DestDefaults.ALL,
            dnd_list,
            Gdk.DragAction.COPY
        )

        label = Gtk.Label(self.utils._("Drag an icon here to create a launcher"))

        w.connect("drag_data_received", self.on_drag_data_received, section, callback_restart)
        w.vbox.pack_start(label, True, True, 10)

        appfinderbtn = Gtk.Button(label=self.utils._("Search for applications"))
        appfinderbtn.connect("button_press_event", appfinder)
        w.action_area.pack_start(appfinderbtn, True, True, 0)
        appfinderbtn.show()
        label.show()

        ret = w.run()
        if ret == Gtk.ResponseType.DELETE_EVENT:
            w.destroy()

    def on_drag_begin(self, widget, _c):
        context = widget.get_style_context()
        context.add_class('onhover')
        self.widget_drag = widget
        self.widget_rec = widget.get_allocation()

    def on_drag_motion(self, layout, context, x, y, timestamp):

        x_in_grid = x - self.APP_MARGIN
        y_in_grid = y - self.APP_MARGIN

        rec = self.widget_drag.get_allocation()
        children = layout.get_children()

        move_x = None
        if x_in_grid < self.APP_SIZE:
            move_x = int(self.APP_MARGIN)
        if x_in_grid > self.APP_SIZE and x_in_grid < self.APP_SIZE * 2:
            move_x = int(self.APP_MARGIN + self.APP_SIZE)
        elif x_in_grid > self.APP_SIZE * 2 and x_in_grid < self.APP_SIZE * 3:
            move_x = int(self.APP_MARGIN + self.APP_SIZE * 2)
        elif x_in_grid > self.APP_SIZE * 3 and x_in_grid < self.APP_SIZE * 4:
            move_x = int(self.APP_MARGIN + self.APP_SIZE * 3)
        elif x_in_grid > self.APP_SIZE * 4:
            move_x = int(self.APP_MARGIN + self.APP_SIZE * 4)

        move_y = None
        if y_in_grid < rec.width:
            move_y = int(self.APP_MARGIN)
        if y_in_grid > rec.width and y_in_grid < rec.width * 2:
            move_y = int(self.APP_MARGIN + rec.width)
        elif y_in_grid > self.APP_SIZE * 2:
            move_y = int(self.APP_MARGIN + rec.width * 2)

        if move_x == None or move_y == None:
            return
        if rec.x == move_x and rec.y == move_y:
            return

        greatest_x = 0
        greatest_y = 0
        for child in children:
            _rec = child.get_allocation()
            if _rec.x > greatest_x:
                greatest_x = _rec.x
            if _rec.y > greatest_y:
                greatest_y = _rec.y

        x_for_greatest_y = 0
        for child in children:
            _rec = child.get_allocation()
            if _rec.y != greatest_y:
                continue
            if _rec.x > x_for_greatest_y:
                x_for_greatest_y = _rec.x

        if move_x > greatest_x or move_y > greatest_y:
            return
        if move_y == greatest_y and move_x > x_for_greatest_y:
            return

        go_right = True
        go_down = True
        if rec.x > move_x:
            go_right = False
        if rec.y > move_y:
            go_down = False

        change_line = False
        if rec.y != move_y:
            change_line = True

        for child in children:
            if child == self.widget_drag:
                continue
            _rec = child.get_allocation()

            if change_line and go_down and _rec.y <= move_y:
                if _rec.y == move_y and _rec.x > move_x:
                    continue
                if rec.y == _rec.y and _rec.x < rec.x:
                    continue
                if _rec.y + self.APP_SIZE < move_y:
                    continue
                if _rec.x == self.APP_MARGIN:
                    layout.move(
                        child,
                        self.APP_MARGIN + self.APP_SIZE * 4,
                        _rec.y - self.APP_SIZE
                    )
                    continue
                layout.move(
                    child,
                    _rec.x - self.APP_SIZE,
                    _rec.y
                )
                continue

            if change_line and not go_down and _rec.y >= move_y:
                if _rec.y == move_y and _rec.x < move_x:
                    continue
                if rec.y == _rec.y and _rec.y > rec.y:
                    continue
                if rec.y == _rec.y and _rec.x > rec.x:
                    continue
                if _rec.y > rec.y:
                    continue

                if _rec.x == self.APP_MARGIN + self.APP_SIZE * 4:
                    layout.move(
                        child,
                        self.APP_MARGIN,
                        _rec.y + self.APP_SIZE
                    )
                    continue
                layout.move(
                    child,
                    _rec.x + self.APP_SIZE,
                    _rec.y
                )
                continue

            if not change_line and go_right and _rec.x == move_x and _rec.y == move_y:
                layout.move(
                    child,
                    _rec.x - self.APP_SIZE,
                    _rec.y
                )
            if not change_line and not go_right and _rec.x == move_x and _rec.y == move_y:
                layout.move(
                    child,
                    _rec.x + self.APP_SIZE,
                    _rec.y
                )
        layout.move(
            self.widget_drag,
            move_x,
            move_y
        )

    def on_drag_drop(self, layout, _c, x, y, time, section):
        src_rec_x = self.widget_rec.x - self.APP_MARGIN
        src_rec_y = self.widget_rec.y - self.APP_MARGIN
        dest_rec = self.widget_drag.get_allocation()
        dest_rec_x = dest_rec.x - self.APP_MARGIN
        dest_rec_y = dest_rec.y - self.APP_MARGIN

        src_x_len = int(src_rec_x / self.APP_SIZE)
        src_y_len = int(src_rec_y / self.APP_SIZE) * 5
        src_index = src_x_len + src_y_len

        dest_x_len = int(dest_rec_x / self.APP_SIZE)
        dest_y_len = int(dest_rec_y / self.APP_SIZE) * 5
        dest_index = dest_x_len + dest_y_len

        context = self.widget_drag.get_style_context()
        context.remove_class("onhover")

        new_list = copy.copy(layout.get_children())
        new_list.insert(dest_index, new_list.pop(src_index))
        new_cycle = iter(copy.copy(new_list))

        new_section = { 'apps': [] }
        while True:
            try:
                label = next(new_cycle)
            except:
                break
            for app in section['apps']:
                if app['name'] == label.get_label():
                    new_section['apps'].append(app)
                    new_list.remove(label)
        self.utils.move_apps(self.config, section, new_section)
