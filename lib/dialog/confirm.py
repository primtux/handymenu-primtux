from gi.repository import Gtk

class ConfirmDialog(Gtk.Dialog):

    def __init__(self, parent, title, message):
        Gtk.Dialog.__init__(
            self,
            title = title,
            parent = parent,
            buttons = (
                Gtk.STOCK_CANCEL,
                Gtk.ResponseType.CANCEL,
                Gtk.STOCK_OPEN,
                Gtk.ResponseType.OK
            )
        )

        self.set_default_size(150, 100)
        box = self.get_content_area()
        if message:
            label = Gtk.Label(message)
            box.add(label)
        self.show_all()
