from gi.repository import Gtk

from .icon_chooser import IconChooserDialog
from ..hm_utils import new_img


class EditAppFrame(Gtk.Frame):
    def __init__(self, label, inner_box):
        Gtk.Frame.__init__(
            self,
            label = label
        )
        self.add(inner_box)
        self.show()


class EditAppDialog(Gtk.Dialog):

    def _set_app_icon(self, widget, event, dialog, section, app, callback_restart):
        chooser = IconChooserDialog(self.utils)
        response = chooser.run()

        if response == Gtk.ResponseType.CANCEL:
            print(self.utils._('Closed, no files selected'))
            chooser.destroy()
            return
        if response == Gtk.ResponseType.OK:
            icon_path = chooser.get_filename()
            self.utils.mod_app_icon(self.config, section, app, icon_path)
            chooser.destroy()
            callback_restart()

    def _set_app_name(self, widget, event, dialog, section, app, callback_restart):
        new_name = widget.get_text().strip()
        if len(new_name) != 0:
            self.utils.mod_app(self.config, section, app, new_name)
            callback_restart()
        dialog.destroy()

    def _delete_app(self, widget, dialog, section, app, callback_restart):
        self.utils.del_app(self.config, section, app)
        callback_restart()
        dialog.destroy()

    def _set_name_frame(self, entry, section, app, callback_restart):
        button_name = Gtk.Button(label = self.utils._('Change'))
        button_name.connect_object('clicked', self._set_app_name, entry, None, self, section, app, callback_restart)
        button_name.show()

        box = Gtk.HBox(False, 2)
        box.pack_start(entry, True, True, 3)
        box.pack_start(button_name, False, False, 0)
        box.show()

        return EditAppFrame(self.utils._('Change the label'), box)

    def _set_icon_frame(self, entry, section, app, callback_restart):
        icon_button = Gtk.Button(label = self.utils._('Change icon'))
        img = Gtk.Image()
        img.set_from_stock(Gtk.STOCK_EDIT, Gtk.IconSize.MENU)
        icon_button.set_image(img)
        icon_button.connect_object('clicked', self._set_app_icon, entry, None, self, section, app, callback_restart)
        icon_button.show()

        return EditAppFrame(self.utils._('Change the application icon'), icon_button)

    def _set_delete_frame(self, section, app, callback_restart):
        delbtn = Gtk.Button(label = self.utils._('Delete'), stock = Gtk.STOCK_DELETE)
        delbtn.connect('clicked', self._delete_app, self, section, app, callback_restart)
        delbtn.show()

        return EditAppFrame(self.utils._('Delete this launcher'), delbtn)

    def __init__(self, utils, config, section, app, callback_restart):
        self.utils = utils
        self.config = config

        Gtk.Dialog.__init__(
            self,
            title = utils._('Edit the launcher')
        )

        entry = Gtk.Entry()
        entry.connect('activate', self._set_app_name, entry, self, section, app, callback_restart)
        entry.show()

        name_frame = self._set_name_frame(entry, section, app, callback_restart)
        self.vbox.pack_start(name_frame, True, True, 0)

        icon_frame = self._set_icon_frame(entry, section, app, callback_restart)

        hbox = Gtk.HBox(False, 2)
        hbox.pack_start(icon_frame, True, True, 0)
        hbox.show();
        self.vbox.pack_start(hbox, True, True, 0)

        delete_frame = self._set_delete_frame(section, app, callback_restart)
        self.vbox.pack_start(delete_frame, True, True, 0)
