from gi.repository import Gtk


class IconChooserDialog(Gtk.FileChooserDialog):
    def __init__(self, utils):
        Gtk.FileChooserDialog.__init__(
            self,
            title = utils._("Choose an icon"),
            action = Gtk.FileChooserAction.OPEN,
            buttons = (
                Gtk.STOCK_CANCEL,
                Gtk.ResponseType.CANCEL,
                Gtk.STOCK_OPEN,
                Gtk.ResponseType.OK
            )
        )
        self.set_current_folder(utils.pixmaps)

        _filter = Gtk.FileFilter()
        _filter.set_name(utils._("Images"))
        _filter.add_mime_type("image/png")
        _filter.add_mime_type("image/jpeg")
        _filter.add_mime_type("image/ico")
        self.add_filter(_filter)
