import os

import gtk


def exec_app(
    widget,
    event,
    data,
    closeafterrun = False,
    verbose = False,
    debug_mode = False
):
    exe = False
    if event.type == gtk.gdk.BUTTON_RELEASE and \
            event.state & gtk.gdk.BUTTON1_MASK:
            exe = True
    elif event.type == gtk.gdk.KEY_PRESS:
        if event.keyval == gtk.keysyms.Return:
            exe = True
    if not exe:
        return
    appname, icon, cmd = data['name'], data['icon'], data['cmd']
    if verbose or debug_mode:
        os.system("xterm {} &".format(cmd.strip()))
    else:
        os.system("{} &".format(cmd.strip()))
    if closeafterrun:
        gtk.main_quit()
