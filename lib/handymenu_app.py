import os
import sys

import yaml

import gettext

import gi
gi.require_version('Gdk', '3.0')  # noqa E402
gi.require_version('Gtk', '3.0')  # noqa E402
from gi.repository import Gtk, Gdk

from .hm_utils import *
from .__version__ import *


class Handymenu():
    def close_application(self, widget, event, data=None):
        # tests nécessaires pour que seul clic-gauche et Entrée soient valables
        if event.type == Gdk.EventType.BUTTON_RELEASE and \
                event.state & Gdk.ModifierType.BUTTON1_MASK:
                Gtk.main_quit()
        if event.type == Gdk.EventType.KEY_PRESS and \
                event.state & Gdk.ModifierType.MOD2_MASK:
           Gtk.main_quit()

    def configure(self, data=None):
        os.system(self.utils.configcmd)
        Gtk.main_quit()

    def exec_app(self, widget, event, data):
        exe = False
        if event.type == Gdk.EventType.BUTTON_RELEASE and \
                event.state & Gdk.ModifierType.BUTTON1_MASK:
                exe = True
        elif event.type == Gdk.EventType.KEY_PRESS: 
            keyval_name = Gdk.keyval_name(event.keyval)
            if keyval_name == 'Return':
                exe = True
        if not exe:
            return
        appname, icon, cmd= data['name'], data['icon'], data['cmd']
        if self.verbose or self.utils.is_debug_mode():
            os.system("xterm {} &".format(cmd.strip()))
        else:
            os.system("{} &".format(cmd.strip()))
        if self.closeafterrun:
            Gtk.main_quit()

    def create_tabs(self):
        if self.config == None:
            return
        self.n_onglets = len(self.config)
        for s in self.config:
            if 'apps' not in s:
                continue
            # Description du bouton
            label_str = ''
            if 'name' in s:
                label_str = s['name']
            label = Gtk.Label(label_str)
            # onglet coloré
            label.modify_fg(
                Gtk.StateFlags.NORMAL,
                Gdk.color_parse("#41B1FF")
            )
            n = 0
            if 'apps' in s and s['apps'] != None:
                n = len(s['apps']) # number of apps to show
            if n <= 4: # si peu d'applications, 1 seule ligne
                r = 1
            else:
                r = 2 # two rows

            if n % 2 == 0:  # nombre d'applis pair ou pas
                c = int(n / r - 1)
            else:
                c = int(n / r) 
            if n > 8 :  # beaucoup d'applications : + de 2 lignes
                c = 4
                r = int(n / c)

            if n == 0 : # empty section
                c = 1

            page = Gtk.Table(rows=r, columns=c, homogeneous=True)
            page.grab_focus()
            page.set_row_spacings(10)
            page.set_col_spacings(10)

            cur = [0,0]
            if n == 0:
                desc = Gtk.Label(self.utils._("This menu is still empty"))
                align = Gtk.Alignment(0.5, 0.5, 0, 0)
                align.add(desc)
                self.onglets.append_page(align, label)
                break
            for a in s['apps']:
                appname, icon, cmd, generic = a['name'], a['icon'], a['cmd'], a['generic']
                # image utilisée dans le bouton
                image = new_img(icon)

                # nom de l'appli
                bapp = Gtk.Button(label=appname)
                bapp.set_always_show_image(True)
                if image:
                    bapp.set_image(image)
                # l'image est au dessus du texte
                bapp.set_image_position(Gtk.Align.END)
                # apparence du bouton
                bapp.set_relief(Gtk.ReliefStyle.NONE)
                bapp.connect("button_release_event", self.exec_app, a)
                bapp.connect("key_press_event", self.exec_app, a)
                # Le bouton survolé change de couleur
                bapp.modify_bg(
                    Gtk.StateFlags.PRELIGHT,
                    Gdk.color_parse("#41B1FF")
                )
                # Description du bouton
                bulledesc = Gtk.Tooltip()
                bulledesc.set_text(generic)

                page.attach(
                    bapp,
                    cur[0],
                    cur[0] + 1,
                    cur[1],
                    cur[1] + 1
                )
                page.set_hexpand(True)
                page.set_vexpand(True)
                if cur[0] < c:
                    cur[0] +=1
                elif cur[0] == c:
                    cur[0] = 0
                    cur[1] += 1
            # pour centrer
            align = Gtk.Alignment()
            align.set(0.5, 0.5, 0, 0)
            align.add(page)

            hbox = Gtk.HBox()
            
            img = None
            if 'icon' in s:
                img = new_img(s['icon'])
            if img:
                hbox.add(img)

            hbox.add(label)
            hbox.show_all()
            self.onglets.append_page(align, hbox)

        #self.onglets.set_tab_label_packing(
        #    align, False, False, False
        #)
        if self.n_onglets > maxonglets: # dyp il aime pas :P
            self.onglets.set_scrollable(True)# dyp y veut pas :P

    def close_after(self, widget):
        self.closeafterrun = widget.get_active()
        try:
            with io.open(self.utils.noclose, 'w', encoding='utf8') as noclose_path:
                yaml.safe_dump(
                    { "close": self.closeafterrun },
                    noclose_path,
                    default_flow_style=False,
                    allow_unicode=True
                )
        except Exception as err:
            print("Soucis d'enregistrement de %s" % self.utils.noclose)
            print(err)

    def make_menu(self):
        """build the menu"""
        # Conteneur principal
        mainbox = Gtk.EventBox()
        self.window.add(mainbox)

        vbox = Gtk.VBox(False, 2)
        vbox.set_border_width(15)
        mainbox.add(vbox)

        # Logo
        image = Gtk.Image()
        image.set_from_file(self.utils.primtuxmenuicon)
        logo = Gtk.EventBox()
        logo.add(image)
        bulledesc = Gtk.Tooltip()
        bulledesc.set_text(self.utils._("Thuban/HandyLinux-Tomasi/PrimTux"))

        # Titre
        title = Gtk.Label()
        title.set_markup('<span size="32000">Handy-PrimTux  </span>')
        title.set_justify(Gtk.Justification.CENTER)
        linkbox = Gtk.EventBox()
        linkbox.add(title)
        bulledesc = Gtk.Tooltip()
        bulledesc.set_text("http://primtux.fr")

        # boutons
        # bouton pour fermer
        closebtn = Gtk.Button()
        croix = Gtk.Image()
        croix.set_from_stock(Gtk.STOCK_CLOSE, Gtk.IconSize.MENU)
        closebtn.set_image(croix)

        closebtn.set_relief(Gtk.ReliefStyle.NONE)
        closebtn.connect("button_release_event", self.close_application)
        closebtn.connect("key_press_event", self.close_application)
        bulledesc = Gtk.Tooltip()
        bulledesc.set_text(self.utils._("Close"))

       # fermer ou pas
        closeafterbtn = Gtk.CheckButton()
        closeafterbtn.connect("toggled", self.close_after)
        closeafterbtn.set_active(self.closeafterrun)
        bulledesc = Gtk.Tooltip()
        bulledesc.set_text(self.utils._("Close after execution"))
        
        # configuration 
        qbtn = Gtk.Button()
        image = Gtk.Image()
        image.set_from_stock(Gtk.STOCK_PREFERENCES, Gtk.IconSize.MENU)
        qbtn.set_image(image)
        qbtn.set_relief(Gtk.ReliefStyle.NONE)
        qbtn.connect_object("clicked", self.configure, None)
        bulledesc = Gtk.Tooltip()
        bulledesc.set_text(self.utils._("Configure"))

        # boite à boutons 
        btnbox = Gtk.VBox(False, 0)
        btnbox.pack_start(closebtn, True, True, 0)
        if self.utils.appname == "prof":
            btnbox.pack_start(qbtn, True, True, 0)
        else:
            btnbox.pack_start(closeafterbtn, True, True, 0)
        btnbox.show();

        # Boite d'en haut
        topbox = Gtk.HBox(False, 0)
        topbox.pack_start(logo, True, True, 0)
        topbox.pack_start(btnbox, False, False, 0)

        vbox.pack_start(topbox, True, True, 0)

        # onglets
        self.onglets = Gtk.Notebook()
        self.onglets.set_tab_pos(Gtk.PositionType.TOP)
        self.onglets.set_show_border(False)
        align = Gtk.Alignment()
        align.set(0.5, 0.5, 0, 0)
        align.add(self.onglets)
        vbox.pack_start(align, True, True, 0)

        # Boite d'en bas
        bottombox = Gtk.HBox(True, 0)
        vbox.pack_start(bottombox, False, False, 0)

        # Catégories
        self.create_tabs()

        self.window.show_all()

    def __init__(self, appname, config_path, verbose):
        self.verbose = verbose
        self.utils = Utils(appname, config_path, verbose)
        self.closeafterrun = True
        try:
            with io.open(self.utils.noclose, 'r') as stream:
                self.closeafterrun = yaml.load(stream)["close"]
        except:
            pass
        self.n_onglets = 0
        self.config = self.utils.load_config()
        self.window = Gtk.Window(Gtk.WindowType.TOPLEVEL)
        self.window.connect("delete_event", lambda x,y: Gtk.main_quit())

        self.window.set_title(self.utils.menuname)
        # Give a black border
        self.window.set_border_width(1)
        self.window.set_icon_from_file(self.utils.primtuxmenuicon)

        self.window.set_position(Gtk.WindowPosition.CENTER)
        self.window.set_resizable(False)
        self.window.set_decorated(False)

        self.make_menu()
        # user can change tab focus with keyboard
        self.onglets.grab_focus()


def main(appname, config_path, verbose):
    if verbose:
        print('handymenu %s' % version)
    menu = Handymenu(appname, config_path, verbose)
    Gtk.main()
    return 0
