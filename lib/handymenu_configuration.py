"""
Description :
    Configuration du handymenu
"""

import sys
import os

import gettext
import locale

from os.path import join

import gi
gi.require_version('Gdk', '3.0')  # noqa E402
gi.require_version('Gtk', '3.0')  # noqa E402
from gi.repository import Gtk, Gdk

from .hm_utils import new_img, Utils
from .style import gtk_style
from .dialog.confirm import ConfirmDialog
from .app_config import AppConfig
from .section_config import SectionConfig
from .__version__ import *


class HandymenuConfig():
    TARGET_TYPE_TEXT = 80
    TARGET_TYPE_PIXMAP = 81
    APP_MARGIN = 20
    APP_SIZE = 180
    widget_drag = None
    widget_rec = None


    def close_application(self, widget, event, data=None):
        appname = "-" + str(self.utils.appname)
        if not self.utils.appname:
            appname = "-prof"
        os.system(
            "{} --force &".format(
                join(
                    self.utils.app_path,
                    "handymenu" + appname
                )
            )
        )
        Gtk.main_quit()
        return False

    def restart(self, widget=None, event=None):
        page = self.section_list.get_current_page()
        self.config = self.utils.load_config()
        self.window.destroy()
        self.make_menu()
        if page > len(self.config):
            page = 0
        self.section_list.set_current_page(page)

    def back_to_default(self, widget):
        confirm = ConfirmDialog(
            widget,
            'Confirmation de réinitialisation',
            'Souhaitez-vous vraiment revenir à la configuration initiale ?'
            + '\nCette opération est irréversible.'
        )
        if confirm.run() == Gtk.ResponseType.OK:
            self.utils.set_default_config()
            self.restart()
        confirm.destroy()

    def add_new_section(self, widget):
        name = widget.get_text().strip()
        if len(name) != 0:
            newsec =  {'name' : name, 'id': '', 'apps': [] }
            self._create_section(newsec)
            nb_pages = self.section_list.get_n_pages()
            new_child = self.section_list.get_nth_page(nb_pages - 1)
            self.section_list.reorder_child(new_child, nb_pages - 2)
            self.section_list.set_current_page(
                self.section_list.get_current_page() + 1
            )
            self.section_list.show_all()
            self.utils.add_section(self.config, newsec)

    def on_set_debug_mode(self, widget):
        config = { 'debug': widget.get_active() }
        for c in self.config:
            if 'config' in c:
                c['config'] = config
                self.utils.save_config(self.config)
                return
        self.config.append({'config': config})
        self.utils.save_config(self.config)

    def _hide_show_btn(self, child, nb_pages, current_index, next_index):
        if (type(self.section_list.get_nth_page(1).get_children()[0]) == Gtk.Label):
            return
        thbox = self.section_list.get_nth_page(0).get_children()[0].get_children()[0].get_children()[0]
        secondhbox = self.section_list.get_nth_page(1).get_children()[0].get_children()[0].get_children()[0]
        bhbox = self.section_list.get_nth_page(nb_pages - 2).get_children()[0].get_children()[0].get_children()[0]
        lasthbox = self.section_list.get_nth_page(nb_pages - 3).get_children()[0].get_children()[0].get_children()[0]
        secondhbox.get_children()[3].set_sensitive(True)
        secondhbox.get_children()[4].set_sensitive(True)
        lasthbox.get_children()[3].set_sensitive(True)
        lasthbox.get_children()[4].set_sensitive(True)
        if current_index != 0 and current_index != nb_pages - 2 and current_index != nb_pages - 1:
            currenthbox = self.section_list.get_nth_page(current_index).get_children()[0].get_children()[0].get_children()[0]
            currenthbox.get_children()[3].set_sensitive(True)
            currenthbox.get_children()[4].set_sensitive(True)
        thbox.get_children()[3].set_sensitive(False)
        thbox.get_children()[4].set_sensitive(True)
        bhbox.get_children()[3].set_sensitive(True)
        bhbox.get_children()[4].set_sensitive(False)

    def move_sec(self, app, index):
        nb_pages = self.section_list.get_n_pages()
        current_index = self.section_list.get_current_page()
        next_index = current_index + index
        if next_index == -1:
            return
        if next_index >= nb_pages - 1:
            return
        if next_index == 0:
            self.section_list.reorder_child(self.section_list.get_nth_page(0), 1)
        else:
            self.section_list.reorder_child(app[1], next_index)
        self.utils.move_section(self.config, app[0], index)
        self._hide_show_btn(app[0], nb_pages, current_index, next_index)

    def on_drag_reorder(self, notebook, child, next_index, app):
        nb_pages = self.section_list.get_n_pages()
        current_index = notebook.get_current_page()
        nodes = notebook.get_tab_label(child).get_children()
        if len(nodes) == 1:
            current_name = nodes[0].get_text()
        else:
            current_name = nodes[1].get_text()

        select_app = None
        inc = 0
        config_index  = 0
        for app in self.config:
            if app['name'] == current_name:
                config_index = inc
                select_app = app
                break
            inc = inc + 1
        notebook.reorder_child(self.addbox, -1)
        self._hide_show_btn(child, nb_pages, current_index, next_index)
        self.utils.move_section_of(
            self.config,
            select_app,
            current_index - config_index
        )

    def _create_section(self, s):
        if 'name' not in s:
            return
        nb_apps = len(s['apps'])
        label = Gtk.Label(s['name'])
        hbox = Gtk.HBox()

        img = None
        if 'icon' in s:
            img = new_img(s['icon'])
        if img:
            hbox.pack_start(img, False, True, 0)

        hbox.pack_end(label, True, True, 0)
        hbox.show_all()

        applist = Gtk.VBox()
        scrolled_window = Gtk.ScrolledWindow()
        scrolled_window.set_policy(
            Gtk.PolicyType.AUTOMATIC,
            Gtk.PolicyType.AUTOMATIC
        )
        scrolled_window.set_size_request(900, 700)
        self.section_list.append_page(scrolled_window, hbox)
        self.section_list.set_tab_reorderable(scrolled_window, True)

        # boutons de config
        hb = Gtk.HBox(False, 10)
        addbtn = Gtk.Button(self.utils._('Add an application'))
        addi = Gtk.Image()
        addi.set_from_stock(Gtk.STOCK_ADD, Gtk.IconSize.BUTTON)
        addbtn.set_image(addi)
        addbtn.connect_object('clicked', self.app.add, s, self.window, self.restart)
        if nb_apps >= 15:
            addbtn.set_tooltip_text(
                'Le nombre d\'applications '
                + 'par section est limité à %s' % nb_apps
            )
            addbtn.set_sensitive(False)
        else:
            addbtn.set_sensitive(True)

        chiconbtn = Gtk.Button(self.utils._('Change icon'))
        chgi = Gtk.Image()
        chgi.set_from_stock(Gtk.STOCK_EDIT, Gtk.IconSize.MENU)
        chiconbtn.set_image(chgi)
        chiconbtn.connect_object(
            'clicked', self.section.set_icon, s, hbox, s['name'], self.section_list
        )

        delbtn = Gtk.Button(self.utils._('Delete this section'))
        deli = Gtk.Image()
        deli.set_from_stock(Gtk.STOCK_DELETE, Gtk.IconSize.MENU)
        delbtn.set_image(deli)
        delbtn.connect_object('clicked', self.section.delete, s, self.section_list)

        upbtn = Gtk.Button(self.utils._('Move section up'))
        upi = Gtk.Image()
        upi.set_from_stock(Gtk.STOCK_GO_UP, Gtk.IconSize.MENU)
        upbtn.set_image(upi)
        upbtn.connect_object(
            'clicked',
            self.move_sec,
            (s, scrolled_window),
            -1
        )
        downbtn = Gtk.Button(self.utils._('Move section down'))
        downi = Gtk.Image()
        downi.set_from_stock(Gtk.STOCK_GO_DOWN, Gtk.IconSize.MENU)
        downbtn.set_image(downi)
        downbtn.connect_object(
            'clicked',
            self.move_sec,
            (s, scrolled_window), 
            +1
        )

        hb.pack_start(addbtn, False, False, False)
        hb.pack_start(chiconbtn, False, False, False)
        hb.pack_start(delbtn, False, False, False)
        hb.pack_start(upbtn, False, False, False)
        hb.pack_start(downbtn, False, False, False)
        if s in self.config and self.config.index(s) == 0:
            upbtn.set_sensitive(False)
        if s in self.config and self.config.index(s) == len(self.config) - 1:
            downbtn.set_sensitive(False)
        applist.pack_start(hb, False, True, 10)
        n = 0
        if 'apps' in s and s['apps'] != None:
            n = len(s['apps']) # number of apps to show
        if n <= 4: # si peu d'applications, 1 seule ligne
            r = 1
        else:
            r = 2 # two rows
        c = 4
        if n > 8 :  # beaucoup d'applications : + de 2 lignes
            c = 4
            r = n/c
        if n == 0 : # empty section
            c = 1

        targs = [Gtk.TargetEntry.new('dummy', Gtk.TargetFlags.SAME_APP, 1)]
        layout = Gtk.Layout()

        layout.drag_dest_set(Gtk.DestDefaults.HIGHLIGHT | Gtk.DestDefaults.DROP | Gtk.DestDefaults.MOTION, targs, Gdk.DragAction.MOVE)
        layout.connect('drag-motion', self.app.on_drag_motion)
        layout.connect('drag-drop', self.app.on_drag_drop, s)

        cur = [0, 0]
        if n == 0:
            applist.pack_start(layout, False, True, 10)
            scrolled_window.add_with_viewport(applist)
            return

        for a in s['apps']:
            appname, icon, cmd= a['name'], a['icon'], a['cmd']

            image = new_img(icon)

            bapp = Gtk.Button(label=appname)
            bapp.set_always_show_image(True)
            if image:
                bapp.set_image(image)
            bapp.set_tooltip_text('double-cliquer pour éditer')

            #l'image est au dessus du texte
            bapp.set_image_position(Gtk.PositionType.TOP)
            # apparence du bouton
            bapp.set_relief(Gtk.ReliefStyle.NONE)
            bapp.connect('button-press-event', self.app.edit, s, a, self.restart)
            bapp.connect('drag-begin', self.app.on_drag_begin)
            bapp.drag_source_set(
                Gdk.ModifierType.BUTTON1_MASK,
                targs,
                Gdk.DragAction.MOVE
            )
            bapp.set_size_request(self.APP_SIZE, self.APP_SIZE)
            layout.put(
                bapp,
                int(self.APP_MARGIN + cur[0] * self.APP_SIZE),
                int(self.APP_MARGIN + cur[1] * self.APP_SIZE)
            )
            if cur[0] < c:
                cur[0] += 1
            elif cur[0] == c:
                cur[0] = 0
                cur[1] += 1
        applist.pack_start(layout, True, True, 0)
        scrolled_window.add_with_viewport(applist)

    def make_entrylist(self):
        self.section_list = Gtk.Notebook()
        self.section_list.set_tab_pos(Gtk.PositionType.TOP)
        for s in self.config:
            self._create_section(s)

        addlabel = Gtk.Image()
        addlabel.set_from_stock(Gtk.STOCK_ADD, Gtk.IconSize.MENU)
        bulledesc = Gtk.Tooltip()
        bulledesc.set_text(self.utils._('Add a section'))
        self.addbox = Gtk.VBox()
        instruction = Gtk.Label(self.utils._(
            'Name of the new section: '
        ))
        entry = Gtk.Entry()
        entry.connect("activate", self.add_new_section)
        self.addbox.pack_start(instruction, False, True, 3)
        self.addbox.pack_start(entry, False, False, 20)

        addbtn = Gtk.Button(self.utils._('Add a section'))
        addi = Gtk.Image()
        addi.set_from_stock(Gtk.STOCK_ADD, Gtk.IconSize.MENU)
        addbtn.set_image(addi)
        addbtn.connect_object('clicked', self.add_new_section, entry)
        self.addbox.pack_start(addbtn, False, False, 10)
        self.section_list.append_page(self.addbox, addlabel)

        searchtab = self.section_list.get_nth_page(0)
        self.section_list.connect(
            'page-reordered',
            self.on_drag_reorder,
            searchtab
        )
        self.mainbox.pack_start(self.section_list, False, False, False)

    def make_menu(self):
        """build the menu"""
        self.window = Gtk.Window(Gtk.WindowType.TOPLEVEL)
        self.window.connect("delete_event", self.close_application)

        self.window.set_title(
            "Configuration du handymenu : session {}".format(
                self.utils.appname
            )
        )
        self.window.set_border_width(0)

        # Conteneur principal
        self.mainbox = Gtk.VBox(False, 10)
        self.mainbox.set_border_width(10)

        # configuration principale
        self.make_entrylist()

        # conteneur pour les boutons
        btnbox = Gtk.HBox(True, 2)
        self.mainbox.pack_start(btnbox, False, False, 0)

        debug_check = Gtk.CheckButton("Debug")
        debug_check.connect(
            'toggled',
            self.on_set_debug_mode
        )
        debug_check.set_active(self.utils.is_debug_mode())
        btnbox.pack_start(debug_check, False, False, False)

        defaultbtn = Gtk.Button(label = self.utils._("Reset"))
        resetimg = Gtk.Image()
        resetimg.set_from_stock(Gtk.STOCK_REDO, Gtk.IconSize.BUTTON)
        defaultbtn.set_image(resetimg)
        defaultbtn.connect_object(
            "clicked", self.back_to_default, self.window
        )
        btnbox.pack_start(defaultbtn, False, False, False)

        savebtn = Gtk.Button(
            label = self.utils._("Quit"),
            stock=Gtk.STOCK_CLOSE
        )
        savebtn.connect_object(
            "clicked",
            self.close_application,
            self.window, None
        )
        btnbox.pack_start(savebtn, False, False, False)

        self.window.add(self.mainbox)
        self.window.set_default_size(1000, 730)
        self.window.show_all()

    def __init__(self, appname, config_path, verbose):
        self.utils = Utils(appname, config_path, verbose)
        self.config = self.utils.load_config()

        self.app = AppConfig(self.utils, self.config, self.APP_MARGIN, self.APP_SIZE)
        self.section = SectionConfig(self.utils, self.config)
        self.make_menu()


def main(appname, config_path, verbose):
    gtk_style()
    if verbose:
        print('handymenu %s' % version)
    menu = HandymenuConfig(appname, config_path, verbose)
    Gtk.main()
    return 0
