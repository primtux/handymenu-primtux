#!/usr/bin/python
# coding: utf8

import sys
from os.path import exists
from lib.hm_utils import Utils

if __name__ == "__main__":
    if len(sys.argv) < 4:
        print("pas assez d'arguments")
        exit(0)
    if (sys.argv[2] not in ['add', 'remove']):
        print("l'argument 2 doit être add ou remove")
        exit(0)
    conf_file = sys.argv[3]
    if not exists(conf_file):
        print(
            'Ce fichier "%s" n\'existe pas !' % conf_file
        )
        exit(0)
    utils = Utils(sys.argv[1], None, None)
    if sys.argv[2] == 'add':
        utils.add_config(conf_file)
    else:
        utils.remove_config(conf_file)

